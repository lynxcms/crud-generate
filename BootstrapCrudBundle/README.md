# Cómo generar un CRUD 

## Creación de Módulo

1. Ejecutar el comando `php bin/console crud-generate:doctrine:crud --entity=NombreBundle:Entidad --with-write --overwrite` y seguir los pasos del asistente

    * Ingresa el nombre del Bundle (se debe incluir nombre del bundle seguido de la entidad a la que se quiere crear), ejemplo: AppBundle:Entidad

    * Indica que deseamos generar los archivos de escritura. Escribimos **'yes'** en la consola.

    * El resto de los valores queda por defecto. Se debe pulsar  **Enter** hasta que finalice el proceso.

    * Copie la consulta **findAllEntidad** en el respositorio de la entidad correspondiente

```
    ....
    use Doctrine\ORM\Tools\Pagination\Paginator;

    .......

    public function findAllEntidad($currentPage = 1, $pageSize = 10, $dataSearch, $sortField = null, $sortDirection = null) {
        $em = $this->getEntityManager();
        
        if (null !== $sortField) {
            if (empty($sortDirection) || !in_array(strtoupper($sortDirection), array('ASC', 'DESC'))) {
                $sortDirection = 'DESC';
            }
        }else{
            //Ordenamiento por defecto
            $sortDirection = 'DESC';
        }
        //Campo de ordenamiento por defecto
        if (!$sortField) {
            $sortField = 'id';
        }
        $dql = "select c from LynxBundle:Entidad c WHERE c.nombre like :nombre ORDER BY c.$sortField $sortDirection";
        
        
        $query = $em->createQuery($dql)
                       ->setFirstResult($pageSize * ($currentPage - 1))
                       ->setMaxResults($pageSize);
        $query->setParameter('nombre', '%' . $dataSearch . '%');
        //$results = $query->getResult();
        $results = new Paginator($query, $fetchJoinCollection = true);
        
        $totalItems = count($results);
        $pagesCount = ceil($totalItems / $pageSize);
        
        return array('results'=>$results, 'totalItems'=>$totalItems, 'pagesCount'=>$pagesCount);
    }

```    

**Nota:** En la consulta de arriba se debe cambiar la palabra **"Entidad"** por el nombre real creado. Tambien se debe ajustar el query de ser necesario.

2. Mueva la carpeta en la ruta `app/Resources/views/carpeta_con_nombre_entidad` y a la ruta del bundle `src/Bundle/Resources/views/Carpeta_Entidad/`

3. Este proceso se debe realizar cada vez que realices la generación de módulos por la consola.


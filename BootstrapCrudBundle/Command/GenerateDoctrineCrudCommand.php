<?php

namespace CrudGenerate\BootstrapCrudBundle\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Command\Command;
use Sensio\Bundle\GeneratorBundle\Command\GenerateDoctrineCrudCommand as GenerateDoctrineCrudCommandMain;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Sensio\Bundle\GeneratorBundle\Generator\DoctrineCrudGenerator;

/**
 * Generates a CRUD for a Doctrine entity.
 *
 * @author Piotr Pasich <piotr.pasich@xsolve.pl>
 */
class GenerateDoctrineCrudCommand extends GenerateDoctrineCrudCommandMain
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setDefinition(array(
                new InputArgument('entity', InputArgument::OPTIONAL, 'The entity class name to initialize (shortcut notation)'),
                new InputOption('entity', '', InputOption::VALUE_REQUIRED, 'The entity class name to initialize (shortcut notation)'),
                new InputOption('route-prefix', '', InputOption::VALUE_REQUIRED, 'The route prefix'),
                new InputOption('with-write', '', InputOption::VALUE_NONE, 'Whether or not to generate create, new and delete actions'),
                new InputOption('format', '', InputOption::VALUE_REQUIRED, 'Use the format for configuration files (php, xml, yml, or annotation)', 'annotation'),
                new InputOption('overwrite', '', InputOption::VALUE_NONE, 'Do not stop the generation if crud controller already exist, thus overwriting all generated files'),
            ))
            ->setDescription('Generador de Crud para el Lynx')
            ->setHelp(<<<EOT
                        Funciones basadas en el comando <info>doctrine:generate:crud</info>.

                        Por defecto este comando solo genera las vistas de lista (list) y mostrar (Show). 
                        <info>php app/console doctrine:generate:crud --entity=AcmeBlogBundle:Post --route-prefix=post_admin</info>

                        Usando la opción --with-write se genera la vista de agregar, editar y borrar. 
                        <info>php app/console doctrine:generate:crud --entity=AcmeBlogBundle:Post --route-prefix=post_admin --with-write</info>

                        Cada archivo generado se basa en una plantilla. 
                        Existen plantillas predeterminadas, pero se pueden sobrescribir colocando plantillas personalizadas en una de las siguientes ubicaciones, por orden de prioridad:

                        <info>BUNDLE_PATH/Resources/SensioGeneratorBundle/skeleton/crud
                        APP_PATH/Resources/SensioGeneratorBundle/skeleton/crud</info>

                        y

                        <info>__bundle_path__/Resources/SensioGeneratorBundle/skeleton/form
                        __project_root__/app/Resources/SensioGeneratorBundle/skeleton/form</info>

                        Para saber más de la estructra en https://github.com/sensio/SensioGeneratorBundle/tree/master/Resources/skeleton 
EOT
            )
            ->setName('crud-generate:generate:crud')
            ->setAliases(array('crud-generate:doctrine:crud'))
        ;
    }

    protected function getSkeletonDirs(BundleInterface $bundle = null)
    {
        
        $skeletonDirs = array();

        if (isset($bundle) && is_dir($dir = $bundle->getPath().'/Resources/SensioGeneratorBundle/skeleton')) {
            $skeletonDirs[] = $dir;
        }

        if (is_dir($dir = $this->getContainer()->get('kernel')->getRootdir().'/Resources/SensioGeneratorBundle/skeleton')) {
            $skeletonDirs[] = $dir;
        }

        $bundleDir = dirname($this->getContainer()->get('kernel')->locateResource('@CrudGenerateBootstrapCrudBundle/Command/GenerateDoctrineCrudCommand.php'));
        $skeletonDirs[] = $bundleDir . '/../Resources/skeleton';
        $skeletonDirs[] = $bundleDir . '/../Resources';

        return $skeletonDirs;
    }
}